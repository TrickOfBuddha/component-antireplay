package org.shenglong.antireplay;

import java.util.concurrent.TimeUnit;

/**
 * <p>@class {@link AntiReplayLocker}</p>
 * <p>@desc	防重放校验器</p>
 * <p>@author Shadow Xiao</p>
 * <p>@date	2020-07-26 14:49</p>
 * <p>@see <a href="TrickOfBuddha@gmail.com">mailTo: TrickOfBuddha@gmail.com</a></p>
 */
public interface AntiReplayLocker {
    /**
     * 默认锁定秒数
     */
    int DEFAULT_LOCK_SECONDS = 3;

    /**
     * 是否已锁定
     *
     * @param traceId 追踪ID
     * @return true-已锁定
     */
    default boolean isLocked(String traceId) {
        return getTimeToLive(traceId) > 0;
    }

    /**
     * 查询锁定剩余时间
     *
     * @param traceId 追踪ID
     * @return 生存时间 [时间单位与{@link #lock(String, long, TimeUnit)}相同]
     */
    long getTimeToLive(String traceId);

    /**
     * 根据默认锁定时间，上防重放锁
     *
     * @param traceId 关键词
     */
    default void lock(String traceId) {
        lock(traceId, DEFAULT_LOCK_SECONDS, TimeUnit.SECONDS);
    }

    /**
     * 根据给定秒数上防重放锁
     *
     * @param traceId 关键词
     * @param seconds 过期秒数
     */
    default void lock(String traceId, long seconds) {
        lock(traceId, seconds, TimeUnit.SECONDS);
    }

    /**
     * 上防重放锁
     *
     * @param traceId  追踪ID
     * @param timeout  过期时间
     * @param timeUnit 时间单位
     */
    void lock(String traceId, long timeout, TimeUnit timeUnit);

    /**
     * 解除防重放锁
     *
     * @param traceId 追踪ID
     */
    void unlock(String traceId);
}
