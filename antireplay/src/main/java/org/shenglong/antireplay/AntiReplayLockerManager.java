package org.shenglong.antireplay;

import java.util.concurrent.TimeUnit;

/**
 * {@link AntiReplayLocker}管理器
 *
 * @author 聖龍
 * @date 2024/2/6 16:26
 * @see <a href="shenglong.xiao@qq.com">mailTo: shenglong.xiao@qq.com</a>
 */
public class AntiReplayLockerManager {
    /**
     * 防重放锁
     */
    private AntiReplayLocker locker;

    public static boolean isLocked(String traceId) {
        AntiReplayLocker lockerInstance = getInstance().locker;
        return lockerInstance != null && lockerInstance.isLocked(traceId);
    }

    public static long getTimeToLive(String traceId) {
        AntiReplayLocker lockerInstance = getInstance().locker;
        return lockerInstance == null ? 0 : lockerInstance.getTimeToLive(traceId);
    }

    public static void lock(String traceId) {
        AntiReplayLocker lockerInstance = getInstance().locker;
        if (lockerInstance != null) {
            lockerInstance.lock(traceId);
        }
    }

    public static void lock(String traceId, long seconds) {
        AntiReplayLocker lockerInstance = getInstance().locker;
        if (lockerInstance != null) {
            lockerInstance.lock(traceId, seconds);
        }
    }

    public static void lock(String traceId, long timeout, TimeUnit timeUnit) {
        AntiReplayLocker lockerInstance = getInstance().locker;
        if (lockerInstance != null) {
            lockerInstance.lock(traceId, timeout, timeUnit);
        }
    }

    public static void unlock(String traceId) {
        AntiReplayLocker lockerInstance = getInstance().locker;
        if (lockerInstance != null) {
            lockerInstance.unlock(traceId);
        }
    }

    public static void init(AntiReplayLocker locker) {
        if (locker == null) {
            throw new IllegalArgumentException("could not set null locker");
        }
        getInstance().locker = locker;
    }

    private static AntiReplayLockerManager getInstance() {
        return AntiReplayLockerManagerHolder.INSTANCE;
    }

    private static class AntiReplayLockerManagerHolder {
        private static final AntiReplayLockerManager INSTANCE = new AntiReplayLockerManager();
    }

    private AntiReplayLockerManager() {
    }
}
