package org.shenglong.antireplay.redis;

import org.shenglong.antireplay.AntiReplayLocker;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.StringUtils;

import java.util.concurrent.TimeUnit;

/**
 * <p>@class {@link RedisAntiReplayLocker}</p>
 * <p>@desc	Redis防重放校验器</p>
 * <p>@author Shadow Xiao</p>
 * <p>@date	2020-07-26 15:07</p>
 * <p>@see <a href="TrickOfBuddha@gmail.com">mailTo: TrickOfBuddha@gmail.com</a></p>
 */
public class RedisAntiReplayLocker implements AntiReplayLocker {
    /**
     * Redis操作器
     */
    private final StringRedisTemplate stringRedisTemplate;
    /**
     * 防重放锁前缀
     */
    private static final String LOCKER_PREFIX = ":ANTI-REPLAY:";
    /**
     * 防重放锁名称
     */
    private static final String LOCKER_VALUE = "ANTI_REPLAY_LOCK";
    /**
     * 防重放锁的执行服务
     */
    private final String serviceName;

    public RedisAntiReplayLocker(StringRedisTemplate stringRedisTemplate) {
        this(stringRedisTemplate, "SERVICE");
    }

    public RedisAntiReplayLocker(StringRedisTemplate stringRedisTemplate, String serviceName) {
        if (stringRedisTemplate == null) {
            throw new IllegalArgumentException("redis template could not be null");
        }
        if (!StringUtils.hasText(serviceName)) {
            throw new IllegalArgumentException("service name could not be blank");
        }
        this.stringRedisTemplate = stringRedisTemplate;
        this.serviceName = serviceName;
    }

    @Override
    public long getTimeToLive(String traceId) {
        if (!StringUtils.hasText(traceId)) {
            return 0;
        }
        Long timeToLive = stringRedisTemplate.getExpire(buildRedisKey(traceId));
        return timeToLive == null || timeToLive <= 0 ? 0 : timeToLive;
    }

    @Override
    public void lock(String traceId, long timeout, TimeUnit timeUnit) {
        if (!StringUtils.hasText(traceId) || timeout <= 0) {
            return;
        }
        stringRedisTemplate.opsForValue().set(buildRedisKey(traceId), LOCKER_VALUE, timeout, timeUnit);
    }

    @Override
    public void unlock(String traceId) {
        if (!StringUtils.hasText(traceId)) {
            return;
        }
        stringRedisTemplate.delete(buildRedisKey(traceId));
    }

    private String buildRedisKey(String traceId) {
        return serviceName + LOCKER_PREFIX + traceId;
    }
}
