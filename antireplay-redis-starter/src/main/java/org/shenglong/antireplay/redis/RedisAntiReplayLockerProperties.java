package org.shenglong.antireplay.redis;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.time.Duration;
import java.util.List;

/**
 * 分布式锁属性
 *
 * @author 聖龍
 * @date 2022/3/29 下午3:44
 * @see <a href="shenglong.xiao@qq.com">mailTo: shenglong.xiao@qq.com</a>
 */
@ConfigurationProperties(prefix = "spring.data.redis")
public class RedisAntiReplayLockerProperties {
    /**
     * Database index used by the connection factory.
     */
    private int database = 0;
    /**
     * Connection URL. Overrides host, port, and password. User is ignored. Example:
     * redis://user:password@example.com:6379
     */
    private String url = "redis://127.0.0.1:6379";
    /**
     * Login username of the redis server.
     */
    private String username;
    /**
     * Login password of the redis server.
     */
    private String password;
    /**
     * Read timeout.
     */
    private Duration timeout = Duration.ofMillis(3000);
    /**
     * Connection timeout.
     */
    private Duration connectTimeout = Duration.ofSeconds(10);
    /**
     * Client name to be set on connections with CLIENT SETNAME.
     */
    private String clientName;
    /**
     * 防重放锁
     */
    private AntiReplayLocker locker;
    /**
     * 集群配置
     */
    private RedisCluster cluster;

    /**
     * 分布式锁配置
     */
    public static class AntiReplayLocker {
        private static final AntiReplayLocker NULL = new AntiReplayLocker();
        /**
         * 是否启用
         */
        private boolean enabled = true;
        /**
         * 锁执行者
         */
        private String executor = "SERVICE";

        public boolean isEnabled() {
            return enabled;
        }

        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }

        public String getExecutor() {
            return executor;
        }

        public void setExecutor(String executor) {
            this.executor = executor;
        }
    }

    /**
     * 集群配置
     */
    public static class RedisCluster {

        /**
         * Comma-separated list of "host:port" pairs to bootstrap from. This represents an
         * "initial" list of cluster nodes and is required to have at least one entry.
         */
        private List<String> nodes;

        public List<String> getNodes() {
            return this.nodes;
        }

        public void setNodes(List<String> nodes) {
            this.nodes = nodes;
        }
    }


    public int getDatabase() {
        return database;
    }

    public void setDatabase(int database) {
        this.database = database;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Duration getTimeout() {
        return timeout;
    }

    public void setTimeout(Duration timeout) {
        this.timeout = timeout;
    }

    public Duration getConnectTimeout() {
        return connectTimeout;
    }

    public void setConnectTimeout(Duration connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public AntiReplayLocker getLocker() {
        return locker == null ? AntiReplayLocker.NULL : locker;
    }

    public void setLocker(AntiReplayLocker locker) {
        this.locker = locker;
    }

    public RedisCluster getCluster() {
        return cluster;
    }

    public void setCluster(RedisCluster cluster) {
        this.cluster = cluster;
    }
}
