package org.shenglong.antireplay.redis;

import jakarta.annotation.Resource;
import org.shenglong.antireplay.AntiReplayLocker;
import org.shenglong.antireplay.AntiReplayLockerManager;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * <p>@class {@link RedisAntiReplayConfig}</p>
 * <p>@desc	Redis 防重放器配置</p>
 * <p>@author Shadow Xiao</p>
 * <p>@date	2020-07-26 15:18</p>
 * <p>@see <a href="TrickOfBuddha@gmail.com">mailTo: TrickOfBuddha@gmail.com</a></p>
 */
@EnableConfigurationProperties(RedisAntiReplayLockerProperties.class)
@ConditionalOnProperty(prefix = "spring.data.redis.locker", name = "enabled", havingValue = "true")
@SpringBootConfiguration
public class RedisAntiReplayConfig {
    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @Resource
    private RedisAntiReplayLockerProperties properties;

    @Bean("redisAntiReplayLocker")
    @ConditionalOnMissingBean
    public AntiReplayLocker antiReplayLocker() {
        String executor = properties.getLocker().getExecutor();
        RedisAntiReplayLocker redisAntiReplayLocker = new RedisAntiReplayLocker(stringRedisTemplate, executor);
        AntiReplayLockerManager.init(redisAntiReplayLocker);
        return redisAntiReplayLocker;
    }
}
