package org.shenglong.antireplay.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RedisAntiReplayApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedisAntiReplayApplication.class, args);
    }

}
