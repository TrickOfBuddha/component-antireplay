package org.shenglong.antireplay.redis;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.shenglong.antireplay.AntiReplayLockerManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * <p>@class {@link Tester}</p>
 * <p>@author Shadow Xiao</p>
 * <p>@date	2020-07-27 11:02</p>
 * <p>@see <a href="TrickOfBuddha@gmail.com">mailTo: TrickOfBuddha@gmail.com</a></p>
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class Tester {

    @Test
    public void test() throws InterruptedException {
        String key1 = "testKey1";
        assertEquals(0, AntiReplayLockerManager.getTimeToLive(key1));
        assertFalse(AntiReplayLockerManager.isLocked(key1));
        AntiReplayLockerManager.lock(key1, 2, TimeUnit.SECONDS);
        assertTrue(AntiReplayLockerManager.isLocked(key1));
        Thread.sleep(2000L);
        assertFalse(AntiReplayLockerManager.isLocked(key1));


        String key2 = "testKey2";
        assertEquals(0, AntiReplayLockerManager.getTimeToLive(key2));
        assertFalse(AntiReplayLockerManager.isLocked(key2));
        AntiReplayLockerManager.lock(key2, 10, TimeUnit.SECONDS);
        assertTrue(AntiReplayLockerManager.isLocked(key2));
        AntiReplayLockerManager.unlock(key2);
        assertFalse(AntiReplayLockerManager.isLocked(key2));
    }
}
